/**
 * Phân biệt var, const, let?
 *
 * var: hỗ trợ hoisting (tự khai báo ngầm tên biến lên trên đầu)
 * let: không hỗ trợ hoisting và không thể khai báo biến cùng tên trên cùng 1 scope. Nếu khác scope mà khai báo biến cùng tên thì ES6 tách thành 2 biến riêng biệt.
 * const: tương tự như let không hỗ trợ hoisting, khi khai báo 2 hằng số ở 2 scope khác nhau thì sẽ phân biệt, hằng số không thể gán lại giá trị.
 */

//hoisting

let title = "cybersoft";
{
  let title = "cybersoft 123";
  console.log(title);
}
console.log(title);
const gg = "https://google.com";
{
  const gg = "https://google.com.vn";
  console.log(gg);
}
/**
 * function trong javascript
 */
main();
// declaration function: hỗ trợ hoisting
function main() {
  console.log("run code");
}

// expression function: không hỗ trợ hoisting
var main = function () {
  console.log("run code 2");
};
/**
 * context: ngữ cảnh (this)
 * mặc định: là window
 * object (trong phương thức của object): this là object đó
 * function: this trong function sẽ đại diện cho object được tạo ra từ function đó (instance)
 * => khi xung đột ngữ cảnh thì this sẽ tính theo ngữ cảnh gần nhất, nếu có ngữ cảnh cho biến đó thì this sẽ trở về windows.
 */
console.log(this);
window.document.title = "Hello";
console.log(window.innerWidth);
console.log(window.innerHeight);
let sinhVien = {
  maHV: "1",
  hoTen: "Nguyen Van A",
  hienthiThongTin: function () {
    let hienThi = () => {
      console.log(this.maHV);
    };
    hienThi();
  },
};
sinhVien.hienthiThongTin();
function SanPham() {
  this.maSP = "";
  this.tenSP = "";
  this.gia = "";
  this.hienthiThongTinSP = function () {
    console.log(this.maSP);
    console.log(this.tenSP);
    console.log(this.gia);
  };
}
let sp1 = new SanPham();
sp1.maSP = "1";
sp1.tenSP = "Iphone 1";
sp1.gia = "1000$";
sp1.hienthiThongTinSP();

let sp2 = new SanPham();
sp2.maSP = "2";
sp2.tenSP = "Iphone 2";
sp2.gia = "2000$";
sp2.hienthiThongTinSP();
/**
 * Arrow function:
 * - Không hỗ trợ từ khóa this
 * - Không hỗ trợ hoisting (giống expression function)
 * - Viết tắt:
 * + khi arrow function có 1 tham số thì có thể viết: = thamSo => {}
 * + khi hàm có 1 lệnh return thì bỏ luôn {}: thamSo => gia_tri_return
 * + khi hàm có 1 lệnh return về object thì ta thay {return} bằng ()
 */
//ES5:
function hello() {
  console.log("Hello");
}
// ES6:
// let sayHelloEs6 = (demo) => {
//   console.log("hello");
// };
let demo = "alo";
// ES6 viết tắt:
let sayHelloEs6 = (demo) => `hello Cybersoft ${demo}`;
// lệnh return object
let createStudent = (id, name) => ({
  id: id,
  name: name,
});
let res = sayHelloEs6("BC 44");
console.log(res);
/**
 * ví dụ 1: xây dựng sự kiện cho 3 nút button => khi click vào nút nào thì sẽ hiển thị ra giá trị innerHTML tương ứng.
 */
var arrButton = document.querySelectorAll(".button");
//[button0,button1,button2]
for (let i = 0; i < arrButton.length; i++) {
  let button = arrButton[i];
  button.onclick = function () {
    console.log(button.innerHTML);
  };
}
/**
 * Default parameter: gía trị mặc định của hàm
 * nếu gọi hàm mà ta truyền giá trị thì hàm sẽ lấy giá trị chúng ta truyền, khi nào không truyền giá trị thì mới lấy giá trị mặc định
 */
let hienThiThongTinHocVien = (
  hoTen = "Alice",
  namSinh = 2000,
  tuoi = 2023 - namSinh
) => {
  console.log(hoTen);
  console.log(namSinh);
  console.log(tuoi);
};
hienThiThongTinHocVien();
hienThiThongTinHocVien("Bob", 2001, 25);
// rest parameter: hàm nhận nhiều giá trị tham số dưới dạng array
let tinhTong = (...rest) => {
  console.log(rest);
};
tinhTong(1);
tinhTong(2, 3, 5);
tinhTong(1, 2, 3, 4, 5, 6);
/**
 * Spread Operator: Toán tử ... => dùng để sao chép giá trị của object hoặc array khi cần clone ra object hoặc array mới
 * + primitive values: number, string, undefined, null, boolean
 * + reference values: object, array
 */
let sv1 = { id: 5, a: 8, a: 9 };
let sv2 = { ...sv1, hoTen: "Alice" };
sv2.id = 10;
console.log("sv1", sv1);
console.log("sv2", sv2);
let arr1 = [1, 2, 3];
let arr2 = [...arr1, 4];
//arr2.push(4);
console.log(arr1);
console.log(arr2);

// destructuring: bóc tách phần tử trong mảng
let pet = {
  namePet: "Miu",
  age: 3,
  size: "small",
  desc: "ect",
  showInfo: function () {
    console.log("hello");
  },
};
let { namePet, age, showInfo, ...restP } = pet;
console.log(namePet);
console.log(age);
console.log(restP);
showInfo();

// tuple: mảng hỗn hợp
let mentor = [
  "01",
  "Alice",
  "1998",
  function () {
    console.log("hêllo");
  },
];
let [ma, ten, namSinh, info] = mentor;
console.log("ma", ma);
info();
