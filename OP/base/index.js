function Cat(name, age) {
  this.name = name;
  this.age = age;
}
let Cat1 = new Cat("Miu", 2);
class Dog {
  // hàm khởi tạo, tự động được gọi mỗi khi new 1 đối tượng mới
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}
let dog1 = new Dog("Lucy", 2);
console.log("age", dog1.age);
