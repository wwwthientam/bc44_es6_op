// map

// forEach
export let renderFoodList = (foodArr) => {
  console.log(foodArr);
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, khuyenMai, loaiMon, giaMon, tinhTrang } = item;
    let contentTr = `
    <tr> 
    <td>${maMon}</td>
    <td>${tenMon}</td>
    <td>${loaiMon ? "Chay" : "Mặn"}</td>
    <td>${giaMon}</td>
    <td>${khuyenMai}</td>
    <td>${item.tinhGiaKM()}</td>
    <td>${tinhTrang ? "Còn" : "Hết"}</td>
    <td>
    <button onclick="suaMonAn(${maMon})" class="btn btn-primary">Edit</button>
    <button onclick="xoaMonAn(${maMon})" class="btn btn-danger">Delete</button>
    </td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
  //tbodyFood
};
