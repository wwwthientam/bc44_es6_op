import { renderFoodList } from "./controller-v2.js";
import { Food } from "../v1/model.js";
import { layThongTinTuForm } from "../v1/controller-v1.js";
// fetch data từ server và render
const BASE_URL = "https://64561d822e41ccf1691409cc.mockapi.io/Food";
let fetchFoodlist = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let foodArr = res.data.map((item) => {
        let { name, type, img, desc, status, id, price, discount } = item;
        let food = new Food(id, name, type, discount, img, desc, price, status);
        return food;
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};
// xóa món ăn:
let xoaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMonAn = xoaMonAn;
fetchFoodlist();
//thêm món ăn:
window.themMon = () => {
  let data = layThongTinTuForm();
  console.log(data);
  let newFood = {
    name: data.tenMon,
    type: data.loaiMon,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      fetchFoodlist();
      $("#exampleModal").modal("hide");
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.suaMonAn = (id) => {
  // lấy chi tiết 1 món ăn từ id
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");

      //show thông tin lên form
      let { id, type, price, img, status, desc, discount, name } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount ? "10" : "20";
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
    })
    .catch((err) => {
      console.log(err);
    });
};
