// import { user, a } from "./controller-v1.js";
import hienThiThongTin, { layThongTinTuForm } from "./controller-v1.js";
import { Food } from "./model.js";
// console.log(user);
// console.log(a);
function themMon() {
  let data = layThongTinTuForm();
  let { maMon, tenMon, loaiMon, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    data;
  let food = new Food(
    maMon,
    tenMon,
    loaiMon,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  hienThiThongTin(food);
}
window.themMon = themMon;
