export class Food {
  constructor(
    maMon,
    tenMon,
    loaiMon,
    khuyenMai,
    hinhMon,
    moTa,
    giaMon,
    tinhTrang
  ) {
    this.maMon = maMon;
    this.tenMon = tenMon;
    this.loaiMon = loaiMon;
    this.giaMon = giaMon;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
  }
  tinhGiaKM() {
    return this.giaMon * (1 - this.khuyenMai);
  }
}
