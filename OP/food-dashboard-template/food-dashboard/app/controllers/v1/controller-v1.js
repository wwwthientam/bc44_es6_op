// export let user = "alice";
// export let a = 5;
// export default function sayHello() {
//   console.log("hello");
// }
// 2 loại export: export & export default
// export: sử dụng được nhiều lần trong cùng 1 file ~ trùng tên và dấu {}
// export default: sử dụng 1 lần duy nhất trong file ~ khác tên vẫn được và không có {}

// end demo
export function layThongTinTuForm() {
  let maMon = document.getElementById("foodID").value;
  let tenMon = document.getElementById("tenMon").value;
  let loaiMon = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value * 1;
  let khuyenMai = document.getElementById("khuyenMai").value * 1;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  return {
    //cách viết thu gọn khi dùng key và value trùng tên nhau:
    maMon,
    tenMon,
    loaiMon,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}
export default function showThongTinLenForm(food) {
  document.getElementById("imgMonAn").src = food.hinhMon;
  document.getElementById("spMa").innerText = food.maMon;
  document.getElementById("spTenMon").innerText = food.tenMon;
  document.getElementById("spLoaiMon").innerText = food.loaiMon;
  document.getElementById("spGia").innerText = food.giaMon;
  document.getElementById("spKM").innerText = food.khuyenMai;
  document.getElementById("spGiaKM").innerText = `0`;
  document.getElementById("spTT").innerText = food.tinhTrang;
  document.getElementById("pMoTa").innerText = food.moTa;
}
